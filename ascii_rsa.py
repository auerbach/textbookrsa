#!/usr/bin/env python

'''Encrypt arbitrary blocks of ascii text.

NOTE: This is NOT production code, does not offer some basic
protections one would expect of production cryptography code and thus
should only be used as an example or for exercises. In particular it
is vulnerable to padding attacks.

This is an example extension of the RSA class.
'''

import binascii

from gmpy2 import mpz
from rsa import RSA
from rsa import PrimeInputError
from rsa import EncryptInputError
from rsa import DecryptInputError

__author__ = 'dtauerbach'


# Defines limit on ascii messages that can be encrypted.
MAX_LIMIT = 10000


class AsciiRSA(RSA):
    '''Encrypts/decrypts ascii messages using textbook RSA.'''

    def __init__(self, primes=None):
        '''Disables ability to initialize with primes, sets const vals.

        Raises:
            PrimeInputError
        '''
        if primes:
            raise PrimeInputError('Cannot initialize with primes.')
        # TODO: draw from rsa const to set these hard coded block values
        self.ascii_block_size = 255
        self.decimal_block_size = 620
        self.decimal_format_str = '0620d'
        RSA.__init__(self, primes)

    def encrypt(self, message):
        '''Encrypts ASCII message.

        Raises:
            EncryptInputError
        '''
        self.__validate_message(message)
        n = self.ascii_block_size
        pieces = [message[i:i+n] for i in range(0, len(message), n)]
        output = []
        for piece in pieces:
            decimal_piece = int(binascii.hexlify(piece), 16)
            decimal_cipher = super(AsciiRSA, self).encrypt(decimal_piece)
            block = format(decimal_cipher, self.decimal_format_str)
            output.append(block)
        return ''.join(block for block in output)

    def decrypt(self, ciphertext):
        '''Decrypts decimal ciphertext.

        Raises:
            DecryptInputError
        '''
        # cannot validate message beyond ensuring characters are decimals
        n = self.decimal_block_size
        blocks = [ciphertext[i:i+n] for i in range(0, len(ciphertext), n)]
        pieces = []
        for block in blocks:
            decrypted_block = super(AsciiRSA, self).decrypt(block)
            pieces.append(binascii.unhexlify('%x' % decrypted_block))
        return ''.join(piece for piece in pieces)

    def __validate_message(self, message):
        if not message:
            raise EncryptInputError('Message must not be empty')
        if (not isinstance(message, str) or
                not all(ord(c) < 128 for c in message)):
            raise EncryptInputError('Message must be ascii string')
        if len(message) > MAX_LIMIT:
            raise EncryptInputError('Message too large')


if __name__ == '__main__':
    print 'You must import this module. See module docstring for usage info.'
    sys.exit(1)
