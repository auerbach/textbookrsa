## Background

A playground for writing padding oracles, playing with flaws in prngs, etc.

## Code Example

You must import the modules.

```
>>> import rsa
>>> key_pair = rsa.RSA(primes=[61,53])
>>> message = 417
>>> ciphertext = key_pair.encrypt(message)
>>> ciphertext
917
>>> key_pair.decrypt(ciphertext)
mpz(417)
>>> key_pair.private_key()
(mpz(2753), 3233)
>>> key_pair.public_key()
(65537, 3233)
```

## Installation

The code has been tested with python2.7. On whatever system you are installing (vagrant, virtualenv, etc), you need to install mpz on your system and gmpy2 via pip, the rest should be standard Python library.

## Tests

```
$ python rsa_test.py
$ python ascii_rsa.test.py
```

## Contributors

This repo was created and is used by Dan Auerbach for testing purposes.

## License

This code is licensed under GPLv3.