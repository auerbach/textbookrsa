#!/usr/bin/env python

'''Implementation of textbook RSA algorithm.

WARNING: this is NOT production code, but rather used to demonstrate
the RSA algorithm in its simplest form. Please use it only for
demonstration purposes.

Usage:

>>> import rsa
>>> key_pair = rsa.RSA(primes=[61,53])
>>> message = 417
>>> ciphertext = key_pair.encrypt(message)
>>> ciphertext
917
>>> key_pair.decrypt(ciphertext)
mpz(417)
>>> key_pair.private_key()
(mpz(2753), 3233)
>>> key_pair.public_key()
(65537, 3233)

You can also have the module generate primes for you by not specifying
the primes. The key strength in this case is controlled by
module-level constants.

>>> another_key_pair = rsa.RSA()
>>> len(another_key_pair.public_key()[1])
2048

Warnings:

* This is textbook RSA. There is no padding, and few efficiency optimizations.

* This module relies on gmpy2 which is built using GMP library. The
  mpz type uses this library and is a drop-in replacement for Python's
  native long value.

* Message for encryption is assumed to be a positive integer less than
  2^(RSA_BIT_STRENGTH-1).

'''

import itertools
import random
import sys

from gmpy2 import invert, is_bpsw_prp, mpz

__author__ = 'dtauerbach'


RSA_BIT_STRENGTH = 2048
# Note: the NIST Special Publication on Computer Security (SP 800-78
# Rev 1 of August 2007) does not allow public exponents e smaller than
# 65537, but does not state a reason for this restriction.
EXPONENT = 65537
PRIME_RETRY_LIMIT = 1000000


class Error(Exception):
    pass


class InputError(Error):
    pass


class PrimeInputError(Error):
    pass


class PrimeRetryLimitError(Error):
    pass


class EncryptInputError(Error):
    pass


class DecryptInputError(Error):
    pass


def get_prime(lower_bound, upper_bound):
    '''Generates primes between lower and upper bound.

    Raises:
        PrimeRetryLimitError
    '''
    for i in xrange(PRIME_RETRY_LIMIT):
        # SystemRandom() uses /dev/urandom for non-blocking secure random
        random_offset = random.SystemRandom().random()
        delta = mpz(random_offset * ((upper_bound - lower_bound) / 2))
        num = lower_bound + delta
        if num % 2 == 0:
            num += 1
        if is_bpsw_prp(num):
            return num
    raise PrimeRetryLimitError('Unable to find prime. retry limit reached')


class RSA(object):
    '''Object holding textbook RSA values. '''

    def __init__(self, primes=None):
        '''Initializes primes, exponent, public and private keys.'''
        if primes:
            self.__validate_input(primes)
            self._p, self._q = primes
        else:
            self.strength = RSA_BIT_STRENGTH
            self.__generate_primes()
        self._n = self._p * self._q
        self._mod = (self._p-1) * (self._q-1)
        self._e = EXPONENT
        self._d = invert(self._e, self._mod)

    def private_key(self):
        '''Returns tuple (d, n) representing RSA private key.'''
        return (self._d, self._n)

    def public_key(self):
        '''Returns tuple (e, n) representing RSA public key.'''
        return (self._e, self._n)

    def encrypt(self, plaintext):
        '''Encrypts plaintext using RSA algorithm, returns ciphertext.

        Raises:
            EncryptInputError
        '''
        # Note: isinstance does not work well with mpz
        try:
            assert mpz(plaintext) == plaintext
        except:
            raise EncryptInputError('Incorrect type: %s' % plaintext)
        if plaintext < 0 or plaintext >= self._n:
            raise EncryptInputError('Out of range: %s' % plaintext)
        return pow(plaintext, self._e, self._n)

    def decrypt(self, ciphertext):
        '''Decrypts ciphertext using RSA, returns plaintext.

        NOTE: textbook RSA cannot determine if ciphertext is vaid, and
        will return an integer regardless, so caller must perform
        appropriate validation.

        Raises:
            DecryptInputError
        '''
        try:
            decimal_value = mpz(ciphertext)
        except ValueError:
            raise DecryptInputError('Non decimal value')
        return pow(decimal_value, self._d, self._n)

    def __validate_input(self, primes):
        '''Validates caller input.

        NOTE: this does NOT validate the security of the prime
        numbers, only that they are prime.

        Raises:
            PrimeInputError: non-prime number provided

        '''
        if len(primes) != 2:
            raise InputError('Incorrect number of arguments')
        try:
            for prime in primes:
                assert is_bpsw_prp(prime)
        except AssertionError, e:
            raise PrimeInputError('Init with non-prime value: %s' % prime)

    def __generate_primes(self):
        '''Generates primes with desired bit strength, discarding others'''
        while True:
            lower = 1 << int(RSA_BIT_STRENGTH / 2 - 1)
            upper = 1 << int(RSA_BIT_STRENGTH / 2 + 1)
            self._p = get_prime(lower, upper)
            self._q = get_prime(lower, upper)
            if len(self._p * self._q) == self.strength:
                break


if __name__ == '__main__':
    print 'You must import this module. See module docstring for usage info.'
    sys.exit(1)
