#!/usr/bin/env python

'''Unittest file for ascii rsa module.'''

from gmpy2 import mpz
import ascii_rsa
import unittest

__author__ = 'dtauerbach'


class AsciiRsaTest(unittest.TestCase):
    def setUp(self):
        self.small_message = 'small'
        self.large_message = 'asjdljflkdsajflksadjflksajflkjsadflkjdslkfjdslkfjsadlkfjsalkfdjlksajflksajfjdsflkjdslksajfdlsjdkjsadflkjsadlkfjsaldfjlsajfdlksajdflksajdflkjdslkfjdsalflkdsjflsakdjflksajdflksajdflkjdsalkfjsadlkfjsalkdjflksadjflkdsajflsajdflksajdflksajdflkjsflkjdsalkfjdslkfjsaldkfjldsajflksadjflksadjflkdsajflksajdflkdsajflksadjflksajdflksajdflkjdsakfjsadlfsalkdjflsadjflksadjflksajdlfjdsalkfjsadlfjlsadjfisdajfoisajdfoidsajfoisajdfosadjifjsadoifjsaoidjfoidsajfoisajdfoisadjfoisajdfoijdsfoijdsoifjsadoifjoidsajfoisadjfoidsajfoisajdfoisajdfoisajdfoi'
        # TODO: allow deterministic tests (this is non-deterministic and SLOW)
        self.test_rsa = ascii_rsa.AsciiRSA()

    def test_encrypt_decrypt(self):
        ciphertext = self.test_rsa.encrypt(self.small_message)
        decrypted = self.test_rsa.decrypt(ciphertext)
        self.assertEqual(self.small_message, decrypted)

    def test_bad_input_primes(self):
        with self.assertRaises(ascii_rsa.PrimeInputError) as input_err:
            ascii_rsa.AsciiRSA(primes=[5, 7])

    def test_bad_input_encrypt(self):
        with self.assertRaises(ascii_rsa.EncryptInputError) as encrypt_err:
            self.test_rsa.encrypt(23)


if __name__ == '__main__':
    unittest.main()
