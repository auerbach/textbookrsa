#!/usr/bin/env python

'''Unittest file for rsa module.'''

from gmpy2 import mpz
import rsa
import unittest

__author__ = 'dtauerbach'


class RsaTest(unittest.TestCase):
    def setUp(self):
        self.small_primes = [mpz(61), mpz(53)]
        self.small_message = mpz(417)
        self.test_rsa = rsa.RSA(primes=self.small_primes)

    def test_encrypt_decrypt(self):
        ciphertext = self.test_rsa.encrypt(self.small_message)
        decrypted = self.test_rsa.decrypt(ciphertext)
        self.assertEqual(self.small_message, decrypted)

    def test_bad_input(self):
        with self.assertRaises(rsa.PrimeInputError) as input_err:
            rsa.RSA(primes=[5, 6])

    def test_bad_plaintext(self):
        with self.assertRaises(rsa.EncryptInputError) as encrypt_err:
            self.test_rsa.encrypt(23423432423)


if __name__ == '__main__':
    unittest.main()
